package com.example.countrycodepicker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.hbb20.CountryCodePicker;

public class MainActivity extends AppCompatActivity {

    private CountryCodePicker ccp;
    private EditText editText;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ccp = findViewById(R.id.login_ccp);
        editText = findViewById(R.id.phone);
        button = findViewById(R.id.btn);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = ccp.getSelectedCountryCodeWithPlus(); // check out other functions too

                String number = editText.getText().toString();
                Log.d("testing",number);
                String phone = code.concat(number);
                Toast.makeText(MainActivity.this,phone,Toast.LENGTH_SHORT).show();
            }
        });
    }
}